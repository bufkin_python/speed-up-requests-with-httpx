import time
import requests
from httpx import AsyncClient
from asyncio import run, gather


async def fetch():
    urls = ["https://books.toscrape.com/catalogue/category/books/default_15/page-2.html",
            "https://books.toscrape.com/catalogue/category/books/default_15/page-3.html",
            "https://books.toscrape.com/catalogue/category/books/default_15/page-4.html",
            "https://books.toscrape.com/catalogue/category/books/default_15/page-5.html",
            "https://books.toscrape.com/catalogue/category/books/nonfiction_13/page-2.html",
            "https://books.toscrape.com/catalogue/category/books/nonfiction_13/page-3.html"
            ]

    async with AsyncClient() as client:
        reqs = [client.get(url) for url in urls]
        results = await gather(*reqs)

    print(results)

if __name__ == '__main__':
    start = time.perf_counter()
    run(fetch())
    end = time.perf_counter()

    print(end - start)
